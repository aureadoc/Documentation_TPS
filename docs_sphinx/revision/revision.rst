.. _revision:

Revision History
================

v3.01 (24/11/22)
----------------

- change libusb interface index to open multiple devices

v3.00 (05/05/22)
----------------

- Add Device index in all functions
- Modify all get function to return value by pointer
- Add ``TPS_`` at the beginning of all functions
- Replace functions :
	- TPS_listATdevices() to TPS_listDevices()
	- TPS_openATdevice() to TPS_openDevice()
	- TPS_closeATdevice() to TPS_closeDevice()
- Add Wrapper
- Handle multiple device application

v2.03 (15/05/20)
----------------

- Add functions :
	- SetPumpPower
	- GetPumpPower

v2.02 (21/11/18)
----------------

- Add function :
	- GetLaserPumpCurrentRange

v2.01 (03/01/17)
----------------

- Improvement of internals functions to close the DLL cleaner way 

v2.00 (29/04/16)
----------------

- Comments changing
- Rename functions :
	- GetPplnTemperature
	- GetPplnTemperatureRange
- By :
	- GetPplnTemperatureCmd
	- GetPplnTemperatureCmdRange
- Modifications functions :
	- GetPumpAttRange
	- SetPumpAttenuation
	- GetPumpAttenaution
- Add functions :
	- SetPumpSourceMode
	- GetPumpSourceMode
	- CheckInitialAlarms

v1.01 (13/04/15)
----------------

- Replace functions :
	- OpenSystemTransfer
	- CloseSystemTransfer
- By
	- ListATdevices
	- OpenATdevice
	- CloseATdevice

v1.00 (10/09/14)
----------------

- First release