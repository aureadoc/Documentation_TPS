.. _wrapper:

C++ Wrapper
===========

Wrapper Advantage
~~~~~~~~~~~~~~~~~

A C++ wrapper has been created for several reasons.
	- To make TPS functions easy to use.
	- To allow multiple TPS device control in the same application and at the same time.
	- To link Dynamic Library inside C++ code and not in the project configuration.

.. note::

	Except OpenDevices function, you do not need to specify iDev when using TPS wrapper function.

	For example function ``TPS_getSystemTemperatures(short iDev, double *intTemp, double *pplnTemp)`` can be replace by ``ObjectName.GetSystemTemperatures(double *intTemp, double *pplnTemp)``

C++ code
~~~~~~~~

Here is an example of how to use this wrapper to recover data from 2 TPS :

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "TPS_wrapper.h"
	#include "TPS.h"

	// Select shared library compatible to current operating system
	#ifdef _WIN32
	#define DLL_PATH L"TPS.dll"
	#elif __unix
	#define DLL_PATH "TPS.so"
	#else
	#define DLL_PATH "TPS.dylib"
	#endif

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		double InternalTemp1, PPLNtemp1;
		double InternalTemp2, PPLNtemp2;

		// Instancie the device from wrapper
		TPS_wrapper TPS0(DLL_PATH);
		TPS_wrapper TPS1(DLL_PATH);

		/*    ListDevices function    */
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (TPS0.ListDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0 || numberDevices == 1) {
				cout << endl << "    Please connect AT device !" << endl << endl;
				do {
					delay(500);
					TPS0.ListDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// Open communication with device 0
		printf(" -%u: %s\n", 0, devicesList[0]);
		TPS0.OpenDevice(0);
		printf("\n TPS %d-> Communication Open\n\n", 0);

		// Open communication with device 1
		printf(" -%u: %s\n", 1, devicesList[1]);
		TPS1.OpenDevice(1);
		printf("\n TPS %d-> Communication Open\n\n", 1);

		// Recover Temperatures from both TPS
		TPS0.GetSystemTemperatures(&InternalTemp1, &PPLNtemp1);
		printf("\n\nTPS 0 -> Internal : %1.3f \370C	PPLN : %1.3f \370C ", InternalTemp1, PPLNtemp1);
		TPS1.GetSystemTemperatures(&InternalTemp2, &PPLNtemp2);
		printf("\nTPS 1 -> Internal : %1.3f \370C	PPLN : %1.3f \370C ", InternalTemp2, PPLNtemp2);

		// Wait some time
		delay(2000);

		/*    CloseDevice function    */
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (TPS0.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;
		if (TPS1.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		// Call class destructor
		TPS0.~TPS_wrapper();
		TPS1.~TPS_wrapper();

		return 0;
	}
