.. _api_functions:


Library information
===================

TPS_getLibVersion
-----------------

.. doxygenfunction:: TPS_getLibVersion
   :project: TPS - API guide




Connection Functions
====================

TPS_listDevices
---------------

.. doxygenfunction:: TPS_listDevices
   :project: TPS - API guide

TPS_openDevice
--------------

.. doxygenfunction:: TPS_openDevice
   :project: TPS - API guide

TPS_closeDevice
---------------

.. doxygenfunction:: TPS_closeDevice
   :project: TPS - API guide





Save and Reset Settings
=======================

TPS_saveAllSettings
-------------------

.. doxygenfunction:: TPS_saveAllSettings
   :project: TPS - API guide

TPS_factorySettings
-------------------

.. doxygenfunction:: TPS_factorySettings
   :project: TPS - API guide





Reboot System
=============

TPS_resetSystem
---------------

.. doxygenfunction:: TPS_resetSystem
   :project: TPS - API guide





Device Information
==================

TPS_getSystemVersion
--------------------

.. doxygenfunction:: TPS_getSystemVersion
   :project: TPS - API guide

TPS_getSystemFeature
--------------------

.. doxygenfunction:: TPS_getSystemFeature
   :project: TPS - API guide





Recover Parameters Range
========================

TPS_getPumpAttenuationRange
---------------------------

.. doxygenfunction:: TPS_getPumpAttenuationRange
   :project: TPS - API guide

TPS_getPplnTempCmdRange
-----------------------

.. doxygenfunction:: TPS_getPplnTempCmdRange
   :project: TPS - API guide

TPS_getLaserPumpCurrentRange
----------------------------

.. doxygenfunction:: TPS_getLaserPumpCurrentRange
   :project: TPS - API guide






Set and Get Parameters
======================

TPS_setSamplingTimeMeasurements
-------------------------------

.. doxygenfunction:: TPS_setSamplingTimeMeasurements
   :project: TPS - API guide

TPS_getSamplingTimeMeasurements
-------------------------------

.. doxygenfunction:: TPS_getSamplingTimeMeasurements
   :project: TPS - API guide

TPS_setPumpSourceMode
---------------------

.. doxygenfunction:: TPS_setPumpSourceMode
   :project: TPS - API guide

TPS_getPumpSourceMode
---------------------

.. doxygenfunction:: TPS_getPumpSourceMode
   :project: TPS - API guide

TPS_setPumpAttenuation
----------------------

.. doxygenfunction:: TPS_setPumpAttenuation
   :project: TPS - API guide

TPS_getPumpAttenuation
----------------------

.. doxygenfunction:: TPS_getPumpAttenuation
   :project: TPS - API guide

TPS_setPplnTemperatureCmd
-------------------------

.. doxygenfunction:: TPS_setPplnTemperatureCmd
   :project: TPS - API guide

TPS_getPplnTemperatureCmd
-------------------------

.. doxygenfunction:: TPS_getPplnTemperatureCmd
   :project: TPS - API guide

TPS_setLaserEmissionState
-------------------------

.. doxygenfunction:: TPS_setLaserEmissionState
   :project: TPS - API guide

TPS_getLaserEmissionState
-------------------------

.. doxygenfunction:: TPS_getLaserEmissionState
   :project: TPS - API guide

TPS_setPumpPower
----------------

.. doxygenfunction:: TPS_setPumpPower
   :project: TPS - API guide

TPS_getPumpPower
----------------

.. doxygenfunction:: TPS_getPumpPower
   :project: TPS - API guide

TPS_getPumpPowerCmd
-------------------

.. doxygenfunction:: TPS_getPumpPowerCmd
   :project: TPS - API guide

TPS_getPumpPowerNval
--------------------

.. doxygenfunction:: TPS_getPumpPowerNval
   :project: TPS - API guide





Monitoring Functions
====================

TPS_getCasingTemp
-----------------

.. doxygenfunction:: TPS_getCasingTemp
   :project: TPS - API guide

TPS_getPplnTemperature
----------------------

.. doxygenfunction:: TPS_getPplnTemperature
   :project: TPS - API guide

TPS_getSystemTemperatures
-------------------------

.. doxygenfunction:: TPS_getSystemTemperatures
   :project: TPS - API guide

TPS_checkInitialAlarms
----------------------

.. doxygenfunction:: TPS_checkInitialAlarms
   :project: TPS - API guide

TPS_getSystemInfosAlarms
------------------------

.. doxygenfunction:: TPS_getSystemInfosAlarms
   :project: TPS - API guide