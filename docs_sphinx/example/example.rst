.. _example:

Code Examples
=============

The following section present simple codes in C++ and Python to use TPS device.

Communication
-------------

This first example shows how to list all TPS connected to a computer and how to open and close USB communication. Device information is also recovered in this example.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "TPS.h"

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		char* pch;
		char* next_pch = NULL;
		char version[64];
		char versionParam[3][32];
		char systemName[6];
		memset(version, ' ', 64);
		memset(systemName, '\0', 6);

		/*	listDevices function	*/
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (TPS_listDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0) {
				cout << endl << "	Please connect device !" << endl << endl;
				do {
					delay(500);
					TPS_listDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// If multiple TPS devices are detected, select one else open it
		if (numberDevices > 1) {
			for (int i = 0; i < numberDevices; i++) {
				printf(" -%u: %s\n", i, devicesList[i]);
			}
			cout << endl << "Select device to drive: ";
			cin >> iDev;

			if (TPS_openDevice(iDev) != 0) {
				cout << "Failed to open TPS" << endl;
			}
		}
		else {
			iDev = 0;
			if (TPS_openDevice(iDev) != 0) {
				cout << "Failed to open TPS" << endl;
			}
		}

		// Parameters recovery
		int v = 0;
		pch = secure_strtok(version, ":", &next_pch);
		while (pch != NULL) {
			snprintf((char*)&versionParam[v][0], 32, "%s", pch);
			pch = secure_strtok(NULL, ":", &next_pch);
			v++;
		}
		if (pch != 0) { snprintf((char*)&versionParam[v][0], 32, "%s", pch); }

		// Show system identity
		memcpy(systemName, (char*)&versionParam[2][0] + 3, 3);
		cout << "	AT System       : " << systemName << endl;
		cout << "	Serial number   : " << versionParam[0] << endl;
		cout << "	Product number  : " << versionParam[1] << endl;
		cout << "	Firmware version: " << versionParam[2] << endl;
		cout << endl;

		// Wait some time
		delay(2000);

		/*	CloseDevice function	*/
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (TPS_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		return 0;
	}

Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import TPS wrapper file  
	import TPS_wrapper as TPS 

	# Application main
	def main():
		key = ''
		iDev = c_short(0)
		nDev = c_short()
		devList = []

		# Scan and open selected device
		devList,nDev=TPS.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
		    	devList,nDev=TPS.listDevices()
		    	time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
		    	print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if TPS.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Recover system version
		ret,version = TPS.getSystemVersion(iDev)
		if ret<0: print(" -> failed\n")
		else:print("System version = {} \n".format(version))

		# Wait some time
		time.sleep(2)

		# Close device communication
		TPS.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 


Recover Data
------------

The next example shows how to use TPS to recover internal and PPLN temperatures. You can place the function TPS_getSystemTemperatures in a loop in order to get multiple data.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "TPS.h"

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		double InternalTemp, PPLNtemp;

		/*	listDevices function	*/
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (TPS_listDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0) {
				cout << endl << "	Please connect device !" << endl << endl;
				do {
					delay(500);
					TPS_listDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// If multiple TPS devices are detected, select one else open it
		if (numberDevices > 1) {
			for (int i = 0; i < numberDevices; i++) {
				printf(" -%u: %s\n", i, devicesList[i]);
			}
			cout << endl << "Select device to drive: ";
			cin >> iDev;

			if (TPS_openDevice(iDev) != 0) {
				cout << "Failed to open TPS" << endl;
			}
		}
		else {
			if (TPS_openDevice(iDev) != 0) {
				cout << "Failed to open TPS" << endl;
			}
		}

		// Recover Internal and PPLN Temperatures
		if (TPS_getSystemTemperatures(iDev, &InternalTemp, &PPLNtemp) == 0) {
			printf("\r	Internal : %1.3f \370C	PPLN : %1.3f \370C ", InternalTemp, PPLNtemp);
		}
		else { cout << "! Recovering failed !" << endl; }

		// Wait some time
		delay(2000);

		/*	CloseDevice function	*/
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (TPS_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;
	
		return 0;
	}

Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import TPS wrapper file  
	import TPS_wrapper as TPS

	# Application main
	def main():
		key = ''
		iDev = c_short(0)
		nDev = c_short()
		devList = []

		# Scan and open selected device
		devList,nDev=TPS.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
			    devList,nDev=TPS.listDevices()
			    time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
			    print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if TPS.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Recover Internal and PPLN Temperatures
		ret, val, val1 = TPS.getSystemTemperatures(iDev)
		if ret < 0 : print(" -> failed\n")
		else : print("System temperature = {}°C PPLN Temperature = {}°C\n".format(val, val1))

		# Wait some time
		time.sleep(2)

		# Close device communication
		TPS.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 
		
.. note::

	All function information is available in section :ref:`All Functions`.
