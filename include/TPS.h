////////////////////////////////////////////////////////////////////////////// 
///
///\mainpage			TPS Library
///
///---------------------------------------------------------------------------
/// \section det_sec Details
/// - FileName		:	TPS.h
/// - Dependencies	:   None
/// - Compiler		:   C++
/// - Company		:   Copyright (C) Aurea Technology
/// - Author		:	Matthieu Grovel
///
///	\section des_sec Description
///	This header document provide the prototypes and descriptions of all functions    
///	integrated on both dynamic (.dll) and static libraries (.lib) files.
/// Those functions allows to control the device named "TPS"
/// 
///
/// \section rev_sec Revisions
///
/// v1.0 (10/09/14)	
/// - First release
///
/// v1.1 (13/04/15)
///	- Replace functions:
///		- OpenSystemTransfer
///		- CloseSystemTransfer
///	- By
///		- ListATdevices
///		- OpenATdevice
///		- CloseATdevice
///
/// v2.0 (29/04/16)	
///	- Comments changing
///	- Rename functions:
///		- GetPplnTemperature
///		- GetPplnTemperatureRange
///	- By:
///		- GetPplnTemperatureCmd
///		- GetPplnTemperatureCmdRange
///	- Modifications functions:
///		- GetPumpAttRange
///		- SetPumpAttenuation
///		- GetPumpAttenaution
///	- Add functions:
///		- SetPumpSourceMode
///		- GetPumpSourceMode
///		- CheckInitialAlarms
///
/// v2.1 (03/01/17)	
///	- Improvement of internals functions to close the DLL cleaner way 
///
/// v2.2 (21/11/18)	
///	- Add function:
///		- GetLaserPumpCurrentRange
///
/// v2.3 (15/05/20)    
///	- Add functions:
///		- SetPumpPower
///		- GetPumpPower
/// 
/// v3.0 (16/07/21)
/// - Add Device index in all functions
/// - Modify all get function to return value by pointer
/// - Add TPS_ at the beginning of all functions
/// - Replace functions:
/// 	- TPS_listATdevices() to TPS_listDevices()
/// 	- TPS_openATdevice() to TPS_openDevice()
///		- TPS_closeATdevice() to TPS_closeDevice()
/// - Add Wrapper
/// - Handle multiple device application
/// 
/// v3.01 (24/11/22)
/// - Change libusb interface index to open multiple devices
/// 
/// v3.02 (05/07/23)
/// - Add GetSystemFeature in wrapper
//////////////////////////////////////////////////////////////////////////////

#ifndef _TPS_H
#define _TPS_H

#ifndef DOXYGEN_SHOULD_SKIP_THIS

 // Define LIB_CALL for any platform
#if defined _WIN32 || defined __CYGWIN__
#ifdef WIN_EXPORT
#ifdef __GNUC__
#define LIB_CALL __attribute__ ((dllexport))
#else
#define LIB_CALL __declspec(dllexport) __cdecl
#endif
#else
#ifdef __GNUC__
#define LIB_CALL __attribute__ ((dllimport))
#else
#define LIB_CALL __declspec(dllimport) 
#endif
#endif
#else
#if __GNUC__ >= 4
#define LIB_CALL __attribute__ ((visibility ("default")))
#else
#define LIB_CALL
#endif
#endif

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <Windows.h>
#include "conio.h"
#define delay(x) Sleep(x)
#elif __unix
#include <cstring>
#include <unistd.h>
#define delay(x) usleep(x*1000)
#else
#include <unistd.h>
#define delay(x) usleep(x*1000)
#endif

#ifdef _WIN32 
#define secure_strtok strtok_s
#else
#define secure_strtok strtok_r
#endif	

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#if defined(__cplusplus)
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short TPS_getLibVersion(unsigned short *value)
///	\brief		Get the librarie version
/// \details	Return the version librarie in format 0x0000MMmm \n
///				with: MM=major version \n
///					  mm=minor version			  
///
/// \param		*value	return lib version by pointer \n
///						Format: 0xMMmm		\n
///						with:	MM: major version	\n
///								mm: minor version
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
short LIB_CALL TPS_getLibVersion(unsigned short* value);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short TPS_listDevices(char** devices, short* number)
/// \brief		List Aurea Technology devices connected
/// \details	List Aurea Technology devices connected
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		**devices pointer to buffer which contain list of AT devices connected \n
///						 Min reception buffer size of 64 bytes per device connected to computer. \n
///						 Output format: "-index: deviceName - serialNumber\r\n"   \n
///						 Ex for 2 devices: \n
///						 " -0: TPS - SN_2016xxxxxxx\r\n -1: TPS - SN_2016xxxxxxx\r\n"
///
/// \param		*number pointer to the number of AT devices connected
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    
///		
///
short LIB_CALL TPS_listDevices(char** devices, short* number);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short TPS_openDevice(short iDev)
/// \brief		Open and initialize Aurea Technology device
/// \details	Open and initialize Aurea Technology device
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_openDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short	TPS_closeDevice(short iDev)
///	\brief		Close Aurea Technology device
/// \details	Close Aurea Technology device previously opened. 
///	\note		Mandory to do after each end of system transfer.
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// \return	
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_closeDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getSystemVersion(short iDev, char *version) 
/// \brief		Get system version
/// \details	Get system version: Serial number, product number and firmware version
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		version	Pointer to the buffer which receive the system version. \n
///						String format: SN_'serialNumber':PN_'ProductNumber':'FirmwareVersion'\n
///						The receive buffer size must be of 64 octets min.
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_getSystemVersion(short iDev, char *version);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getSystemFeature(short iDev, short feature, short *option) 
///	\brief		Get system feature
/// \details	Read EEPROM to recovery one feature of system
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function 
///
/// \param		feature 0: optical wavelenght	\n
///						1: wireless connection	
/// 
/// \param		option	pointer to the system feature option (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL TPS_getSystemFeature(short iDev, short feature, short* option);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_saveAllSettings(short iDev)
///	\brief		Save all parameters
/// \details	Save all parameters (counting mode,...).
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_saveAllSettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_factorySettings(short iDev)
///	\brief		System factory settings
/// \details	Set all parameters with factory settings.
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_factorySettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short TPS_setSamplingTimeMeasurements(short iDev, double time)
/// \brief		Set the sampling time of measurements
/// \details	Set the sampling time of measurements between 0.1s to 10s. 
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
/// \param		time	Time value in s (format: double) \n 
///						Value between 0.5 to 10.0s with step of 0.1s
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_setSamplingTimeMeasurements(short iDev, double time);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short TPS_getSamplingTimeMeasurements(short iDev, double *time) 
/// \brief		Get sampling time of measurements
/// \details	Get the sampling time of measurements, between 0.5s to 10s. 
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*time	pointer to the sampling time measurement value (format: double)
///
/// \return	
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getSamplingTimeMeasurements(short iDev, double *time);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_resetSystem(short iDev)
///	\brief		Reset system
///	\details	Reset system
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_resetSystem(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getCasingTemp(short iDev, double *temp)
///	\brief		Get casing system temperature
///	\details	Get casing system temperature
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*temp	pointer to the casing temperature value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_getCasingTemp(short iDev, double* temp);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_checkInitialAlarms(short iDev)
///	\brief		Check initial alarms
///	\details	Command to check if some alarms are initially presents. \n
///				If that is the case the system send initial alarms to be recover \n
///				by the function "GetSystemInfosAlarms".
///	\note		This function is to be sent only one time at start of the application.
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///				
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_checkInitialAlarms(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getSystemInfosAlarms(short iDev, char *msg)
///	\brief		Get system infos and alarms
///	\details	Get active alarms detected by system and useful informations
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
///	\param		msg		Pointer to the buffer which receive the message. \n
///						The receive buffer size must be of 64 octets min.
///
/// \return
///				1 : Alarm detected	   \n
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_getSystemInfosAlarms(short iDev, char *msg);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_setPumpSourceMode(short iDev, short mode)
///	\brief		Set pump source mode
/// \details	Set pump source in External or Internal mode
/// \note		/!\ The system must reach its set temperature when the pump source \n
///				goes from the external mode to internal. \n
///				So the return of the function is just the consideration of the command \n
///				when pump source mode is passed in internal mode, not the correct application of the mode. \n
///				To know if the Internal mode is correctly applied, check the return of  \n
///				the "getSystemInfosAlarms" funtion until receive "Warming-up OK" message. \n
///				Allows to not have a blocking function during the setting temperature. \n
///				In External mode the function return the direct effect, no need to check \n
///				the  previous message because the system do not need to warming-up temperature.
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
/// \param		mode mode type: 0=External or 1=Internal
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_setPumpSourceMode(short iDev, short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPumpSourceMode(short iDev, short *mode) 
///	\brief		Get pump source mode
/// \details	Get pump source mde (External or Internal)
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*mode	pointer to the Pump source mode (short: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getPumpSourceMode(short iDev, short* mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPumpAttenuationRange(short iDev, double *MinVal, double *MaxVal, double *StepVal)
///	\brief		Get pump attenuation range
/// \details	Get the min, max and step values of the pump attenuation available
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
///	\param		MinVal	pointer to the pump attenuation command min value (format: double) 
///
///	\param		MaxVal	pointer to the pump attenuation command max value (format: double)
///
///	\param		StepVal	pointer to the pump attenuation command step value (format: double)
///		
/// \return 
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_getPumpAttenuationRange(short iDev, double *MinVal, double *MaxVal, double *StepVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPumpAttenuation(short iDev, double *dBval) 
/// \brief		Get pump attenuation
/// \details	Get actual pump attenuation value (in dB).
///	\note		Only fro system with optical attenuator
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*dBval	pointer to the PPLN attenuation value (format: double)
///
///	\return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getPumpAttenuation(short iDev, double* dBval);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_setPumpAttenuation(short iDev, double dBval)
///	\brief		Set pump attenuation
/// \details	Set pump attenuation value according to the range available (in dB).
///	\note		Setting possible only in internal pump source and with optical attenuator
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
///	\param		dBval	pump attenuation value en dB \n						
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_setPumpAttenuation(short iDev, double dBval);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPplnTempCmdRange(short iDev, double *MinVal, double *MaxVal, double *StepVal)
///	\brief		Get PPLN temperature command range
///	\details	Get PPLN temperature command range and step for available settings
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
///	\param		MinVal	pointer to the PPLN temperature command min value (format: double) 
///
///	\param		MaxVal	pointer to the PPLN temperature command max value (format: double)
///
///	\param		StepVal	pointer to the PPLN temperature command step value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_getPplnTempCmdRange(short iDev, double *MinVal, double *MaxVal, double *StepVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPplnTemperatureCmd(short iDev, double *temp) 
///	\brief		Get current PPLN temperature command 
/// \details	Get current PPLN temperature command value (in Degree Celsius).
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*temp	pointer to the PPLN command temperature value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getPplnTemperatureCmd(short iDev, double* temp);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_setPplnTemperatureCmd(short iDev, double temp)
///	\brief		Set PPLN temperature command
/// \details	Set PPLN temperature command value according to the range available (in Degree Celsius). 
/// \note		/!\ The system must reach the PPLN temperature consign and so can takes many minutes. \n
///				So the return of the function is just the consideration of the command, \n
///				not the correct application of the PPLN temperature. \n
///				To know if the temperature is correctly reached, check the return of  \n
///				the "getSystemInfosAlarms" funtion until receive "Warming-up OK" message. \n
///				Allows to not have a blocking function during the setting temperature. \n
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
///	\param		temp PPLN temperature command in Degree Celsius (format: double) 
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_setPplnTemperatureCmd(short iDev, double temp);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPplnTemperature(short iDev, double *temp) 
///	\brief		Get actual PPLN temperature
/// \details	Get actual PPLN temperature value (in Degree Celsius).
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*temp	pointer to the PPLN temperature value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getPplnTemperature(short iDev, double* temp);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_setLaserEmissionState(short iDev, short state)
///	\brief		Set laser emission state
/// \details	Set laser emission in ON or OFF state
///	\note		Setting possible only in internal pump source
/// 
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
/// \param		state emission state: 0=OFF or 1=ON
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_setLaserEmissionState(short iDev, short state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getLaserEmissionState(short iDev, short* state) 
///	\brief		Get laser emission state
/// \details	Get laser emission state (ON or OFF)
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*state	pointer to the state of the laser (format: short)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
short LIB_CALL TPS_getLaserEmissionState(short iDev, short* state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getSystemTemperatures(short iDev, double *intTemp, double *pplnTemp)
///	\brief		Get system temperatures
///	\details	Get all temperatures of the system with auto storage and recovery. \n
/// \note		The function is not blocking and use a FIFO of 100 values for \n
///				each temperatures. That way it allows to TPS_get temperature status during 
///				warming-up process. \n
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
///	\param		*intTemp	pointer to the internal temperature value (format: double)
///
///	\param		*pplnTemp	pointer to the PPLN temperature value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_getSystemTemperatures(short iDev, double *intTemp, double *pplnTemp);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getLaserPumpCurrentRange(short iDev, double *MinVal, double *MaxVal)
///	\brief		Get laser pump current range
///	\details	Get laser pump current range
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
///
///	\param		MinVal	pointer to the current min value (format: double) 
///
///	\param		MaxVal	pointer to the current max value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL TPS_getLaserPumpCurrentRange(short iDev, double *MinVal, double *MaxVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_setPumpPower(short iDev, unsigned short index)
///	\brief		Set pump power
///	\details	Adjust laser pump power
/// \note       Only for system with no optical attenuator
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
///	\param		index	pump power index to set (between 0 to n)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_setPumpPower(short iDev, unsigned short index);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPumpPower(short iDev, unsigned short* index)
///	\brief		Get pump power
/// \details	Get current laser pump power
/// \note       Only for system with no optical attenuator
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*index	pointer to the index value of the Pump (format: unsigned short)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getPumpPower(short iDev, unsigned short* index);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPumpPowerCmd(short iDev, unsigned short index, unsigned short* pwr, unsigned short* current)
///	\brief		Get pump power Cmd
/// \details	Get current laser pump power Cmd
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		index	index of the Pump value
/// 
/// \param		*pwr	pointer to the power value of the Pump (format: unsigned short)
/// 
/// \param		*current	pointer to the current value of the Pump (format: unsigned short)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getPumpPowerCmd(short iDev, unsigned short index, unsigned short* pwr, unsigned short* current);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short TPS_getPumpPowerNval(short iDev, unsigned short* n)
///	\brief		Get pump power number of possible values
/// \details	Get pump power number of possible values
///
/// \param		iDev	Device index indicate by "TPS_listDevices" function
/// 
/// \param		*n	pointer to the current number of value (format: unsigned short)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL TPS_getPumpPowerNval(short iDev, unsigned short* n);

#if defined(__cplusplus)
}
#endif

#endif
